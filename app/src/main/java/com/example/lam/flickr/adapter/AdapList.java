package com.example.lam.flickr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.lam.flickr.R;
import com.example.lam.flickr.model.IMG;
import com.example.lam.flickr.model.ItemImg;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lam on 7/17/2018.
 */

public class AdapList extends RecyclerView.Adapter<AdapList.ViewHolderList> {

    private Context mContext;
    private List<IMG> mList;

    public AdapList(Context mContext, ArrayList<IMG> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    public static class ViewHolderList extends RecyclerView.ViewHolder {
        public ImageView mImageView, mImageView1, mImageView2, mImageView3;
        public TextView tv1, tv2, tv3, tv4;

        public ViewHolderList(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.imvlist);
            mImageView1 = (ImageView) itemView.findViewById(R.id.imvlist1);
            mImageView2 = (ImageView) itemView.findViewById(R.id.imvlist2);
            mImageView3 = (ImageView) itemView.findViewById(R.id.imvlist3);
            tv4 = (TextView) itemView.findViewById(R.id.txt);
            tv3 = (TextView) itemView.findViewById(R.id.txt3);
            tv2 = (TextView) itemView.findViewById(R.id.txt2);
            tv1 = (TextView) itemView.findViewById(R.id.txt1);
        }
    }

    @Override
    public ViewHolderList onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tem_imglist,
                parent, false);
        ViewHolderList vh = new ViewHolderList(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolderList holder, int position) {
        final IMG imgList = mList.get(position);
        ItemImg item0 = imgList.getItemImgList().get(0);
        ItemImg item1 = imgList.getItemImgList().get(1);
        ItemImg item2 = imgList.getItemImgList().get(2);
        ItemImg item3 = imgList.getItemImgList().get(3);

        Glide.with(mContext)
                .load(item0.getUrl())
                .thumbnail(0.5f)
                .into(holder.mImageView);

        Glide.with(mContext)
                .load(item1.getUrl())
                .thumbnail(0.5f)
                .into(holder.mImageView1);

        Glide.with(mContext)
                .load(item2.getUrl())
                .thumbnail(0.5f)
                .into(holder.mImageView2);

        Glide.with(mContext)
                .load(item3.getUrl())
                .thumbnail(0.5f)
                .into(holder.mImageView3);


        holder.tv1.setText("Image");
        holder.tv2.setText("Image");
        holder.tv3.setText("Image");
        holder.tv4.setText("Image");
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void addAll(List<IMG> newList) {
        mList.addAll(newList);
    }

    public void clear() {
        mList.clear();
    }
}

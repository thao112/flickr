package com.example.lam.flickr.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.lam.flickr.adapter.Adap;
import com.example.lam.flickr.adapter.AdapList;
import com.example.lam.flickr.inteface.ILoadMore;
import com.example.lam.flickr.R;
import com.example.lam.flickr.recycleView.EndlessRecyclerViewScrollListener;
import com.example.lam.flickr.url.UrlManager;
import com.example.lam.flickr.model.IMG;
import com.example.lam.flickr.model.IMGList;
import com.example.lam.flickr.model.Item;
import com.example.lam.flickr.model.ItemImg;
import com.google.gson.Gson;
import com.reginald.swiperefresh.CustomSwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by lam on 7/10/2018.
 */

public class Fragment_Home extends Fragment {

    /*
    public void AAA() {
        RecyclerView mRecyclerView;
        Adap mRcvAdapter;
//    List<String> data;

        @Nullable
        @Override
        public View onCreateView (LayoutInflater inflater, @Nullable ViewGroup container, Bundle
        savedInstanceState){
            View view = inflater.inflate(R.layout.fragment_home, container, false);

//        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
            // 2. set layoutManger
//        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

            // 3. create an adapter
//        mRcvAdapter = new Adap(data);
//        mRcvAdapter = new Adap();
//        // 4. set adapter
//        mRecyclerView.setAdapter(mRcvAdapter);
//        // 5. set item animator to DefaultAnimator
//        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

            return view;
        }
    }
    */

    private static final String TAG = Fragment_Home.class.getSimpleName();
    private static final int COLUMN_NUM = 4;
    private static final int ITEM_PER_PAGE = 100;

    private RequestQueue mRq;
    private RecyclerView mRecyclerView, rcc;
    private LinearLayoutManager mLayoutManager, mLayoutManagerG;
    private CustomSwipeRefreshLayout mSwipeRefreshLayout;
    ProgressBar progressBar;

    private Adap mAdapter;
    private AdapList adapList;
    private Gson gson;
    private RequestQueue requestQueue;

    private boolean mLoading = false;
    private boolean mHasMore = true;

    Context context;

    private EndlessRecyclerViewScrollListener scrollListener;


    public Fragment_Home(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mRq = Volley.newRequestQueue(getActivity());

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        rcc= (RecyclerView)view.findViewById(R.id.recycler_view2);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManagerG = new LinearLayoutManager(getActivity());
        mLayoutManagerG.setOrientation(LinearLayoutManager.HORIZONTAL);
        rcc.setLayoutManager(mLayoutManagerG);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new Adap(getActivity(), new ArrayList<IMGList>());
        mRecyclerView.setAdapter(mAdapter);

        adapList = new AdapList(getActivity(), new ArrayList<IMG>());
        rcc.setAdapter(adapList);

        mSwipeRefreshLayout = (CustomSwipeRefreshLayout)view.findViewById(R.id.swipe_refresh);
        mSwipeRefreshLayout.setOnRefreshListener(new CustomSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });

        scrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page) {
                Toast.makeText(getActivity(), "loading...", Toast.LENGTH_SHORT).show();
//                startLoading();
                fetchPosts();
            }
        };
        mRecyclerView.addOnScrollListener(scrollListener);
//        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                int totalItem = mLayoutManager.getItemCount();
//                int lastItemPos = mLayoutManager.findLastVisibleItemPosition();
//                if (mHasMore && !mLoading && totalItem - 1 != lastItemPos){
//
//                    startLoading();
//
//                    if (loadMore != null){
//                        loadMore.onLoadMore();
//                    }
//                    isLoading=true;
//                }
//            }
//        });
        rcc.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int totalItem = mLayoutManager.getItemCount();
                int lastItemPos = mLayoutManager.findLastVisibleItemPosition();
                if (mHasMore && !mLoading && totalItem - 1 != lastItemPos){
//                    startLoading2();
                }
            }
        });
        Log.i("DEMo","truyuiiiiuuy");
        fetchPosts();
        startLoading2();
        return view;
    }

    public void refresh(){
        mAdapter.clear();
        fetchPosts();
    }

    private void fetchPosts() {

        final List<IMGList> imgListArrayList = new ArrayList<>();

        for (int i = 0; i<20; i++){
            imgListArrayList.add(new IMGList(new ArrayList<Item>()));
        }

        mLoading = true;
        final int totalItem = mLayoutManager.getItemCount();
        final int page = totalItem / ITEM_PER_PAGE + 1;

        String query = PreferenceManager
                .getDefaultSharedPreferences(getActivity())
                .getString(UrlManager.PREF_SEARCH_QUERY, null);

        final String ENDPOINT = UrlManager.getInstance().getItemUrl(query, page);
        Log.e("ENDPOINT", "eeeee"+ENDPOINT.toString());
//        Toast.makeText(getActivity(), ENDPOINT, Toast.LENGTH_SHORT).show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ILoadMore api = retrofit.create(ILoadMore.class);

        Call<Item> call = api.getItem();

        call.enqueue(new Callback<Item>() {

            @Override
            public void onResponse(Call<Item> call, retrofit2.Response<Item> response) {
//                List<ItemImg> itemList = null;
                List<ItemImg> data = new ArrayList<>();

                Item item = response.body();
                for (int i = 0; i< 100; i++) {

                    String id = item.getPhotos().getPhoto().get(i).getId().toString();
                    String secret = item.getPhotos().getPhoto().get(i).getSecret().toString();
                    String server = item.getPhotos().getPhoto().get(i).getServer().toString();
                    String farm = item.getPhotos().getPhoto().get(i).getFarm().toString();

                    Log.i("NNNN", "NNN : "+id+":"+secret+":"+server+":"+farm);

                      data.add(new ItemImg(id,secret,server,farm));

                }

                int countImage = 0;
                int countItem = 0;

                for(int i = 0; i<100; i++){
                    countImage++;
                    ItemImg itemImg = data.get(i);

                    if (countImage<6 && countItem<20){
                        imgListArrayList.get(countItem).getItemImgList().add(itemImg);
                        if (countImage == 5){
                            countItem++;
                            countImage = 0;
                        }
                    }
                }

//                for (int in = 0; in < 5; in++) {
//                    ItemImg item1 = data.get(in);
//                    int count = 0;
//
//                    for (int j = 0; j < 20; j++) {
//                        imgListArrayList.get(count).getItemImgList().add(item1);
//                        count++;
//                    }
//
//                }

//                ArrayList<String> data = new ArrayList<String>();
//                Log.i("jjj","Item: "+item);
//                Log.i("Item", "ItemList "+itemList);
//


//                for (int i=0; i<100; i++) {
////
//                   ItemImg item1 = data.get(i);
//// 0 5 10
//                   if ( i - 4 < 1){
//                       imgListArrayList.get(0).getItemImgList().add(item1);
//                   }else if (i - 4 < 6) {
//                       imgListArrayList.get(1).getItemImgList().add(item1);
//                   }else if (i - 4 < 11){
//                       imgListArrayList.get(2).getItemImgList().add(item1);
//                   }else if (i - 4 < 16){
//                       imgListArrayList.get(3).getItemImgList().add(item1);
//                   }else if (i - 4 < 21){
//                       imgListArrayList.get(4).getItemImgList().add(item1);
//                   }else if (i - 4 < 26) {
//                       imgListArrayList.get(5).getItemImgList().add(item1);
//                   }else if (i - 4 < 31) {
//                       imgListArrayList.get(6).getItemImgList().add(item1);
//                   } else if (i - 4 < 36){
//                       imgListArrayList.get(7).getItemImgList().add(item1);
//                   }else if (i - 4 < 41){
//                       imgListArrayList.get(8).getItemImgList().add(item1);
//                   }else if (i-4 < 46){
//                       imgListArrayList.get(9).getItemImgList().add(item1);
//                   } else if (i - 4 < 51){
//                       imgListArrayList.get(10).getItemImgList().add(item1);
//                   }else if (i-4 < 56){
//                       imgListArrayList.get(11).getItemImgList().add(item1);
//                   }else if (i-4 < 61){
//                       imgListArrayList.get(12).getItemImgList().add(item1);
//                   } else if (i - 4 < 66){
//                       imgListArrayList.get(13).getItemImgList().add(item1);
//                   }else if (i-4 <71){
//                       imgListArrayList.get(14).getItemImgList().add(item1);
//                   }else if (i-4 < 76){
//                       imgListArrayList.get(15).getItemImgList().add(item1);
//                   }else if (i - 4 < 81){
//                       imgListArrayList.get(16).getItemImgList().add(item1);
//                   }else if (i-4 < 86){
//                       imgListArrayList.get(17).getItemImgList().add(item1);
//                   }else if (i-4 < 91){
//                       imgListArrayList.get(18).getItemImgList().add(item1);
//                   }else if (i - 4 < 96){
//                       imgListArrayList.get(19).getItemImgList().add(item1);
//                   }else if (i-4 < 101){
//                       imgListArrayList.get(20).getItemImgList().add(item1);
//                   }
//                }
                mAdapter.addAll(imgListArrayList);

                Log.i("R","RRR "+data);
//                mAdapter.addAll(imgListArrayList);
                mAdapter.notifyDataSetChanged();

                mLoading = false;
                mSwipeRefreshLayout.refreshComplete();
            }

            @Override
            public void onFailure(Call<Item> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("ERROR", t.getMessage());
            }
        });

    }

//    private void startLoading(){
//        Log.d(TAG, "startLoading");
//
//        final List<IMGList> imgListArrayList = new ArrayList<>();
//
//        for (int i = 0; i<20; i++){
//            imgListArrayList.add(new IMGList(new ArrayList<ItemImg>()));
//        }
//
//        mLoading= true;
//        final int totalItem = mLayoutManager.getItemCount();
//        final int page = totalItem / ITEM_PER_PAGE + 1;
//
//        String query = PreferenceManager
//                .getDefaultSharedPreferences(getActivity())
//                .getString(UrlManager.PREF_SEARCH_QUERY, null);
//
//        String url = UrlManager.getInstance().getItemUrl(query, page);
//
//        Log.d("UTT", "url: "+url);
//
//        Toast.makeText(getActivity(), "url:"+url, Toast.LENGTH_SHORT).show();
//
//
//
//        final JsonObjectRequest request = new JsonObjectRequest(url,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.d(TAG, "onResponse "+ response);
//                        List<ItemImg> result = new ArrayList<>();
//
//                        try {
//                            JSONObject photos = response.getJSONObject("photos");
//                            if (photos.getInt("page") == page){
//                                mHasMore = false;
//                            }
//                            JSONArray photoArr = photos.getJSONArray("photo");
//                            for (int i = 0; i <100; i++){
//                                JSONObject itemObj = photoArr.getJSONObject(i);
//                                ItemImg item = new ItemImg(
//                                        itemObj.getString("id"),
//                                        itemObj.getString("secret"),
//                                        itemObj.getString("server"),
//                                        itemObj.getString("farm")
//                                );
//                                result.add(item);
//
//                            }
//                        }catch (JSONException e){
//
//                        }
////tạo đói tượng item
////                        for (int i = 0; i < 20; i++) {
////                            ItemImg item = new ItemImg();
////                            int count = 0;
////                            for (int c = 0; c < 5; c++) {
////                                imgListArrayList.get(count).getItemImgList().add(item);
////                                count++;
////                            }
////                        }
//
//                        for (int i=0; i<100; i++) {
//
//                           ItemImg item = result.get(i);
//// 0 5 10
//                           if ( i - 4 < 1){
//
//                               imgListArrayList.get(0).getItemImgList().add(item);
//                           }else if (i - 4 < 6) {
//                               imgListArrayList.get(1).getItemImgList().add(item);
//                           }else if (i - 4 < 11){
//                               imgListArrayList.get(2).getItemImgList().add(item);
//                           }else if (i - 4 < 16){
//                               imgListArrayList.get(3).getItemImgList().add(item);
//                           }else if (i - 4 < 21){
//                               imgListArrayList.get(4).getItemImgList().add(item);
//                           }else if (i - 4 < 26) {
//                               imgListArrayList.get(5).getItemImgList().add(item);
//                           }else if (i - 4 < 31) {
//                               imgListArrayList.get(6).getItemImgList().add(item);
//                           } else if (i - 4 < 36){
//                               imgListArrayList.get(7).getItemImgList().add(item);
//                           }else if (i - 4 < 41){
//                               imgListArrayList.get(8).getItemImgList().add(item);
//                           }else if (i-4 < 46){
//                               imgListArrayList.get(9).getItemImgList().add(item);
//                           } else if (i - 4 < 51){
//                               imgListArrayList.get(10).getItemImgList().add(item);
//                           }else if (i-4 < 56){
//                               imgListArrayList.get(11).getItemImgList().add(item);
//                           }else if (i-4 < 61){
//                               imgListArrayList.get(12).getItemImgList().add(item);
//                           } else if (i - 4 < 66){
//                               imgListArrayList.get(13).getItemImgList().add(item);
//                           }else if (i-4 <71){
//                               imgListArrayList.get(14).getItemImgList().add(item);
//                           }else if (i-4 < 76){
//                               imgListArrayList.get(15).getItemImgList().add(item);
//                           }else if (i - 4 < 81){
//                               imgListArrayList.get(16).getItemImgList().add(item);
//                           }else if (i-4 < 86){
//                               imgListArrayList.get(17).getItemImgList().add(item);
//                           }else if (i-4 < 91){
//                               imgListArrayList.get(18).getItemImgList().add(item);
//                           }else if (i - 4 < 96){
//                               imgListArrayList.get(19).getItemImgList().add(item);
//                           }else if (i-4 < 101){
//                               imgListArrayList.get(20).getItemImgList().add(item);
//                           }
//                        }
//
////                        Toast.makeText(getActivity(), result+"", Toast.LENGTH_SHORT).show();
//                        Log.d("DD", "RESULT: "+result+"");
//
//                        mAdapter.addAll(imgListArrayList);
//
////                        adapList.addAll(imgListArrayList2);
//
//                        mAdapter.notifyDataSetChanged();
////                        adapList.notifyDataSetChanged();
//
//                        mLoading = false;
//                        mSwipeRefreshLayout.refreshComplete();
////                        loadData();
//
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//
//
//                    }
//        });
//        request.setTag(TAG);
//        mRq.add(request);
//
//
//    }

    private void startLoading2(){

        final List<IMG> imgListArrayList2 = new ArrayList<>();

        for (int i = 0; i<5; i++){
            imgListArrayList2.add(new IMG(new ArrayList<ItemImg>()));
        }

        mLoading= true;
        int totalItem = mLayoutManager.getItemCount();
        final int page = totalItem / ITEM_PER_PAGE + 1;

        String query = PreferenceManager
                .getDefaultSharedPreferences(getActivity())
                .getString(UrlManager.PREF_SEARCH_QUERY, null);

        final String ENDPOINT = UrlManager.getInstance().getItemUrl(query, page);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ILoadMore api = retrofit.create(ILoadMore.class);

        Call<Item> call = api.getItem();

        call.enqueue(new Callback<Item>() {

            @Override
            public void onResponse(Call<Item> call, retrofit2.Response<Item> response) {
//                List<ItemImg> itemList = null;
                List<ItemImg> data = new ArrayList<>();

                Item item = response.body();
                for (int i = 0; i < 20; i++) {

                    String id = item.getPhotos().getPhoto().get(i).getId().toString();
                    String secret = item.getPhotos().getPhoto().get(i).getSecret().toString();
                    String server = item.getPhotos().getPhoto().get(i).getServer().toString();
                    String farm = item.getPhotos().getPhoto().get(i).getFarm().toString();

                    Log.i("NNNN", "NNN : " + id + ":" + secret + ":" + server + ":" + farm);

//                    String uuRL = "http://farm" + farm + ".static.flickr.com/" + server + "/" + id + "_" + secret + ".jpg";
//                    Log.i("String URL", "URLL "+(i+1)+" "+uuRL);
//
                    data.add(new ItemImg(id, secret, server, farm));

//                    Log.i("jjj","Item: "+item);
//
//                    String n = item.toString();
//                    Log.i("K", "LOGGGG: "+n);
//                    itemList.add();

//                    data.add(n);
                }

                int countImage = 0;
                int countItem = 0;

                for(int i = 0; i< 20; i++){
                    countImage++;
                    ItemImg itemImg = data.get(i);
                    if (countImage<5 && countItem<5){
                        imgListArrayList2.get(countItem).getItemImgList().add(itemImg);
                        if (countImage == 4){
                            countItem++;
                            countImage = 0;
                        }
                    }
                }

//                for (int i=0; i<20; i++) {
//
//                    ItemImg item2 = data.get(i);
//                    // 0 1 2 3, 4 5 6 7, 8 9 10 11, 12 13 14 15, 16 17 18 19, 20
//                    if (i - 3 < 1) {
//                        imgListArrayList2.get(0).getItemImgList().add(item2);
//                    } else if (i - 3 < 5) {
//                        imgListArrayList2.get(1).getItemImgList().add(item2);
//                    } else if (i - 3 < 9) {
//                        imgListArrayList2.get(2).getItemImgList().add(item2);
//                    } else if (i - 3 < 13) {
//                        imgListArrayList2.get(3).getItemImgList().add(item2);
//                    } else if (i - 3 < 17) {
//                        imgListArrayList2.get(4).getItemImgList().add(item2);
//                    }else if (i - 3 < 21) {
//                        imgListArrayList2.get(5).getItemImgList().add(item2);
//                    }
//                }

                Log.d("DD", "RESULT: "+data+"");

                adapList.addAll(imgListArrayList2);

                Log.i("list","list2: "+imgListArrayList2);

                adapList.notifyDataSetChanged();

                mLoading = false;
                mSwipeRefreshLayout.refreshComplete();

            }
                @Override
                public void onFailure(Call<Item> call, Throwable t) {
                    Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.d("ERROR", t.getMessage());
                }
            });

    }

    private void stopLoading(){
        if (mRq != null){
            mRq.cancelAll(TAG);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        stopLoading();
    }

}

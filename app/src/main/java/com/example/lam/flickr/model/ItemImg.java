package com.example.lam.flickr.model;

import java.io.Serializable;

/**
 * Created by lam on 7/12/2018.
 */

public class ItemImg implements Serializable {

    private String id;
    private String secret;
    private String server;
    private String farm;
    private String url;

    public ItemImg(String id, String secret, String server, String farm){
        this.id = id;
        this.secret = secret;
        this.server = server;
        this.farm = farm;
//        this.url = "http://farm" + farm + ".static.flickr.com/" + server + "/" + id + "_" + secret + ".jpg";
    }

    public ItemImg() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public void setFarm(String farm) {
        this.farm = farm;
    }

    public String getUrl() {
        return "http://farm" + farm + ".static.flickr.com/" + server + "/" + id + "_" + secret + ".jpg";
//        return "http://farm1.static.flickr.com/862/28756738827_5faa371201.jpg";
    }

}

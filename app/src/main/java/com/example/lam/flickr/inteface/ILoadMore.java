package com.example.lam.flickr.inteface;

import com.example.lam.flickr.model.Item;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by lam on 7/16/2018.
 */

public interface ILoadMore {
    String BASE_URL = "https://api.flickr.com/services/rest/";
    @GET("?method=flickr.photos.getRecent&api_key=dda3931f7c544bb6eab964644af778db&format=json&nojsoncallback=1&page=1")
    Call<Item> getItem();
}

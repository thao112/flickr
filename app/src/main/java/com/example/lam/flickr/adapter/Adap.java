package com.example.lam.flickr.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.lam.flickr.inteface.ILoadMore;
import com.example.lam.flickr.R;

import com.example.lam.flickr.model.IMGList;
import com.example.lam.flickr.model.ItemImg;

import java.util.List;

/**
 * Created by lam on 7/10/2018.
 */

public class Adap extends RecyclerView.Adapter<Adap.ViewHolder>{

    Context context;
    List<IMGList> mList;

    private List<ItemImg> mDataset;

    private boolean loading = false;

    private ILoadMore mOnLoadMoreListener;

//    private List<String> data = new ArrayList<>();

//    public Adap(List<String> data) {
//        this.data = data;
//    }

    public Adap(Context context, List<IMGList> mList) {
        this.context = context;
        this.mList = mList;
//        this.mOnLoadMoreListener = iLoadMore;
    }

    public void removeLastItem() {
        mDataset.remove(mList.size() - 1);
        notifyDataSetChanged();
    }

    public void update(List<ItemImg> mDataSet) {
        mDataset.addAll(mDataSet);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img1, imv2, imv3, imv4, imv5;
        TextView tv, tv1;

        public ViewHolder(View itemView) {
            super(itemView);
            img1 = (ImageView) itemView.findViewById(R.id.imvB);
            imv2 = (ImageView) itemView.findViewById(R.id.imv1);
            imv3 = (ImageView) itemView.findViewById(R.id.imv2);
            imv4 = (ImageView) itemView.findViewById(R.id.imv3);
            imv5 = (ImageView) itemView.findViewById(R.id.imvCC);
            tv = (TextView) itemView.findViewById(R.id.tvHead);
            tv1 = (TextView) itemView.findViewById(R.id.tvTV);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_img,
                parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (holder instanceof ViewHolder) {

            final IMGList imgList = mList.get(position);

            ItemImg item0 = imgList.getItemImgList().get(0);
            ItemImg item1 = imgList.getItemImgList().get(1);
            ItemImg item2 = imgList.getItemImgList().get(2);
            ItemImg item3 = imgList.getItemImgList().get(3);
            ItemImg item4 = imgList.getItemImgList().get(4);

            Glide.with(context)
                    .load(item0.getUrl())
                    .thumbnail(0.5f)
                    .into(holder.img1);

            Glide.with(context)
                    .load(item1.getUrl())
                    .thumbnail(0.5f)
                    .into(holder.imv2);

            Glide.with(context)
                    .load(item2.getUrl())
                    .thumbnail(0.5f)
                    .into(holder.imv3);

            Glide.with(context)
                    .load(item3.getUrl())
                    .thumbnail(0.5f)
                    .into(holder.imv4);

            Glide.with(context)
                    .load(item4.getUrl())
                    .thumbnail(0.5f)
                    .into(holder.imv5);

//            for (int i=0; i<20; i++){
                holder.tv.setText("Tiêu đề " + (position+1));
                holder.tv1.setText("Đề mục "+ (position+1));
//            }
//        }else {
//            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
//            if (!loading) {
//                if (mOnLoadMoreListener != null) {
//                    mOnLoadMoreListener.onLoadMore(mDataset.get(position));
//                }
//                loading = true;
//            }

        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setLoaded(){
        loading = false;
    }

    public void addAll(List<IMGList> newList) {
        mList.addAll(newList);
    }

//    public void addAll(List<IMGList> newList) {
//        mList.addAll(newList);
//    }

    public void clear() {
        mList.clear();
    }

}


package com.example.lam.flickr;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;

//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.example.lam.flickr.fragment.Fragment_Chat;
import com.example.lam.flickr.fragment.Fragment_Contact;
import com.example.lam.flickr.fragment.Fragment_Home;
import com.example.lam.flickr.fragment.Fragment_Search;

public class MainActivity extends AppCompatActivity {

    ActionBar toolbar;
    BottomNavigationView btv;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    Fragment fragment;

    Fragment_Home home;
    Fragment_Chat chat;
    Fragment_Contact contact;
    Fragment_Search search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = getSupportActionBar();
        btv = (BottomNavigationView)findViewById(R.id.navigationView);

        home = new Fragment_Home();
        contact = new Fragment_Contact();
        chat = new Fragment_Chat();
        search = new Fragment_Search();

        openFragment(home);

        btv.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home:
                        openFragment(home);
//                        item.setIcon(R.drawable.chat);
                        return true;
//                        item.setIcon(R.drawable.homecheck);
                    case R.id.search:
                        openFragment(search);
                        return true;
//                        item.setIcon(R.drawable.searchcheck);
                    case R.id.chat:
                        openFragment(chat);
                        return true;
//                        item.setIcon(R.drawable.chatcheck);
                    case R.id.contact:
                        openFragment(contact);
                        return true;
//                        item.setIcon(R.drawable.contactcheck);
                }
                return true;
            }
        });

    }

    public void openFragment(Fragment fragment) {
        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
//        fragment = null;

        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}

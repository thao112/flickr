package com.example.lam.flickr.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lam on 7/12/2018.
 */

public class IMGList implements Serializable {

    private List<ItemImg> itemImgList= new ArrayList<>();

    public List<ItemImg> getItemImgList() {
        return itemImgList;
    }

    public void setItemImgList(List<ItemImg> itemImgList) {
        this.itemImgList = itemImgList;
    }

    public IMGList(ArrayList<Item> items) {
    }

    public IMGList(List<ItemImg> itemImgList) {
        this.itemImgList = itemImgList;
    }

}

package com.example.lam.flickr.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lam on 7/13/2018.
 */

public class IMG implements Serializable {

    private List<ItemImg> itemImgList2= new ArrayList<>();

    public List<ItemImg> getItemImgList() {
        return itemImgList2;
    }

    public void setItemImgList(List<ItemImg> itemImgList2) {
        this.itemImgList2 = itemImgList2;
    }

    public IMG() {
    }

    public IMG(List<ItemImg> itemImgList2) {
        this.itemImgList2 = itemImgList2;
    }

}
